from django.apps import AppConfig


class TsukkomisConfig(AppConfig):
    name = 'tsukkomis'
