from django.contrib import admin

# Register your models here.
from tsukkomis.models import Tsukkomi

admin.site.register(Tsukkomi)
