from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models


# Create your models here.
from mlh.utils.models import BaseModel
from users.models import User


class Tsukkomi(BaseModel):
    """吐槽模型类"""
    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name='吐槽发布者')
    content = RichTextUploadingField(verbose_name='吐槽内容', max_length=200)
    # image = models.FileField(verbose_name='图片')
    # content_text = models.CharField(verbose_name='吐槽文字',max_length=150)
    like_times = models.IntegerField(verbose_name='点赞次数', default=0)
    share_times = models.IntegerField(verbose_name='分享次数', default=0)
    comment_times = models.IntegerField(verbose_name='评论量', default=0)


    class Meta:
        db_table = 'tb_tsukkomi'
        verbose_name = '吐槽模型类'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "第%s条吐槽" % self.id


class Comment(models.Model):
    """评论模型类"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='评论发布者')
    tsukkomi = models.ForeignKey(Tsukkomi, on_delete=models.CASCADE,related_name="comment", verbose_name='评论的吐槽')
    comment_like_times = models.IntegerField(default=0, verbose_name='评论点赞条数')
    comment_content = models.CharField(max_length=50, verbose_name='评论内容')

    class Meta:
        db_table = 'tb_ts_comment'
        verbose_name = '吐槽评论'
        verbose_name_plural = verbose_name


class CommentLike(models.Model):
    """评论点赞"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, verbose_name='评论')

    class Meta:
        db_table = 'tb_comment_like'
        verbose_name = '用户点赞评论'
        verbose_name_plural = verbose_name


class TsukkomiLike(models.Model):
    """吐槽点赞"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')
    tsukkomi = models.ForeignKey(Comment, on_delete=models.CASCADE, verbose_name='吐槽')

    class Meta:
        db_table = 'tb_tsukkomi_like'
        verbose_name = '用户点赞吐槽'
        verbose_name_plural = verbose_name


class TsukkomiCollect(models.Model):
    """吐槽收藏"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')
    tsukkomi = models.ForeignKey(Comment, on_delete=models.CASCADE, verbose_name='吐槽')

    class Meta:
        db_table = 'tb_tsukkomi_collect'
        verbose_name = '用户收藏吐槽'
        verbose_name_plural = verbose_name
