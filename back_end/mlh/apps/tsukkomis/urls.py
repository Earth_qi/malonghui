from django.conf.urls import url

from tsukkomis.uploads import ImageUploadView
from . import views

urlpatterns = [
    url(r'^tsukkomis/$', views.TsukkomiListView.as_view()),
    url(r'^tsukkomis/(?P<pk>\d+)/$', views.TsukkimiView.as_view()),
    url(r'^tsukkomis/new/$', views.PostTsukkimiView.as_view()),
    url(r'^upload/$', ImageUploadView.as_view()),
]