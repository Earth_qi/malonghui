from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.generics import ListAPIView, GenericAPIView, RetrieveAPIView, CreateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from tsukkomis.models import Tsukkomi
from tsukkomis.serializers import TsukkomiListSerializer, TsukkomiSerialzer, PostTsukkimiSerializer, \
    TsukkomiLikeserizlizer
from users.models import User


class LargeResultsSetPagination(PageNumberPagination):
    """分页功能"""
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100
    # page_size 每页数目
    # page_query_param 前端发送的页数关键字名，默认为"page"
    # page_size_query_paray 前端发送的每页数目关键字名，默认为None
    # max_page_size 前端最多能设置的每页数量

class TsukkomiListView(ListAPIView):
    """
    吐槽列表查询展示
    """
    serializer_class = TsukkomiListSerializer
    queryset = Tsukkomi.objects.all().order_by('-create_time')
    pagination_class = LargeResultsSetPagination  # 指定份额


class TsukkimiView(RetrieveAPIView):
    """
    吐槽详情页展示
    """
    serializer_class = TsukkomiSerialzer
    queryset = Tsukkomi.objects.all()


class PostTsukkimiView(CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = PostTsukkimiSerializer

    # def get(self, request, tsuk_id):
    #     """获取发布者的name 和评论者的name 再一起进行序列化返回"""
    #
    #     tsuk = self.get_object().filter(id=tsuk_id)
    #
    #     user_id = tsuk.user_id
    #     comments = tsuk.comment
    #     username = User.objects.get(id=user_id).username
    #     tsuk["username"]=username
    #     comment_list = []
    #
    #     for comment in comments:
    #         comment_user_id = comment.user_id
    #         comment_username = User.objects.get(id=comment_user_id).username
    #         comment["comment_username"]=comment_username
    #         comment_list.append(comment)
    #
    #     tsuk["comment"] = comment_list
    #     serializer = self.get_serializer(tsuk)

    # def get(self, request, tsuk_id):
    #     # 查询
    #     tsuk = self.get_object().filter(id=tsuk_id)
    #     """
    #     {
    #         "id":1
    #         "user_id":1,
    #         ...
    #         "comment": [
    #             {
    #                 "id":1,
    #                 "user_id":2,
    #                 "comment_likes":10,
    #                 "comment_content": "xxxxxxx"
    #             },
    #             {"id=2",......},
    #             {},
    #             {}
    #         ]
    #     }


class TsukkomiLike(RetrieveAPIView):
    """
    吐槽点赞
    必须是登陆的的用户才能点赞
    获取吐槽id   tsukkomi_id
    获取点赞动作
    如果action=add   吐槽点赞数+1
    如果action=remove 吐槽点赞数-1
    """
    permission_classes = [IsAuthenticated]
    serializer_class = TsukkomiLikeserizlizer

    def put(self,request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_vaild(raise_exception=True)

        id = serializer.validated_data['id']
        action = request.data['action']








