from django.db import models
from users.models import User

# Create your models here.
from mlh.utils.models import BaseModel


class DetailsCategory(BaseModel):
    """头条分类"""
    name = models.CharField(max_length=10,verbose_name='头条分类名称')
    # details = models.ManyToManyField(Details, through='DetailsRelation')

    class Meta:
        db_table = 'tb_details_category'
        verbose_name = '头条分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s' % self.name


class DetailsSpecial(BaseModel):
    """专栏"""
    title = models.CharField(max_length=50,verbose_name='专栏标题')
    special_url = models.CharField(max_length=50, verbose_name='专栏url')
    desc = models.TextField(verbose_name='专栏简介')
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='用户id')

    class Meta:
        db_table = 'tb_details_special'
        verbose_name = '头条专栏'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s' %(self.title)


class Details(BaseModel):
    """头条详情"""
    title = models.CharField(max_length=50,verbose_name='标题')
    user = models.ForeignKey(User,on_delete=models.PROTECT,verbose_name='用户id', related_name='user')
    label = models.CharField(max_length=20,verbose_name='标签')
    is_audit = models.BooleanField(default=False, verbose_name='是否通过审核')
    is_show = models.BooleanField(default=False, verbose_name='是否展示')
    outline = models.TextField(verbose_name='概要')
    detail_text = models.TextField(verbose_name='详情内容')
    category = models.ForeignKey(DetailsCategory, on_delete=models.PROTECT, verbose_name="分类id")
    collect_user = models.ManyToManyField(User, db_table='tb_details_user_collect')

    class Meta:
        db_table = 'tb_details'
        verbose_name = '头条详情'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s:%s'%(self.id,self.title)


class DetailsComments(BaseModel):
    """头条评论"""
    detail = models.ForeignKey(Details,on_delete=models.PROTECT,verbose_name='头条id')
    parent_id = models.IntegerField(default=-1, verbose_name='父级id')
    comment_text = models.TextField(verbose_name='评论内容')
    user = models.ForeignKey(User,verbose_name='用户')

    class Meta:
        db_table = 'tb_details_comments'
        verbose_name = '头条评论'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s：%s'%(self.detail_id,self.parent_id)


# class DetailsRelation(models.Model):
#     """头条分类表和头条列表的中间表"""
#     classify = models.ForeignKey(DetailsCategory,verbose_name='头条分类id')
#     detail = models.ForeignKey(Details,verbose_name='头条id')
#
#     class Meta:
#         db_table = 'tb_details_relation'
#         verbose_name = '头条分类_列表关联表'
#         verbose_name_plural = verbose_name
#
#     def __str__(self):
#         return '%s:%s'%(self.classify,self.detail_id)









