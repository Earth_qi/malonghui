from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from details.models import Details, DetailsCategory, DetailsComments, DetailsSpecial
from details.serializers import DetailsListSerializer, DetailsCommentsSerializer, DetailsCommentsAddSerializer, \
    DetailsSpecialListSerializer, DetailsUserCollectSerializer
from details.models import Details, DetailsCategory
from details.serializers import DetailsListSerializer, DetailsContentSerializer
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated

from details.serializers import DetailsClassSerializer, SaveDetailsSerializer, SaveSpecialSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 20


class DetailsView(ListAPIView):
    """
    头条分类数据显示
    """
    queryset = DetailsCategory.objects.all()
    serializer_class = DetailsClassSerializer


class DetailsPublishView(CreateAPIView):
    """
    创建头条
    """
    permission_classes = [IsAuthenticated]
    serializer_class = SaveDetailsSerializer

    # """前端获取title,cid,label,detail_text;这四个参数能从前端获取，不需要重写create方法"""
    # def create(self, request, *args, **kwargs):
    #     # 获取用户ｉｄ
    #     user_id = self.request.user.id
    #     s = self.request.data['detail_text']
    #     if len(s) < 30:
    #         return Response({'message':'发布的内容字数没达到30'},status=status.HTTP_400_BAD_REQUEST)
    #     outline = s[:30]
    #     # 是否要捕捉异常
    #     request.data['outline'] = outline
    #     request.data['user'] = user_id
    #     return super().create(self,request,*args,**kwargs)


class SaveDetailsSpecialView(CreateAPIView):
    """
    添加专栏
    """
    permission_classes = [IsAuthenticated]
    serializer_class = SaveSpecialSerializer

    # def create(self, request, *args, **kwargs):
    #     # 获取user_id
    #     user_id = self.request.user.id
    #     request.data['user_id'] = user_id
    #     return super().create(request, *args, **kwargs)


class DetailsCommentsView(ListAPIView):
    """
    头条评论显示
    """
    serializer_class = DetailsCommentsSerializer
    # 排序
    filter_backends = (OrderingFilter,)
    ordering_fields = ('create_time', )

    def get_queryset(self, *args, **kwargs):
        detail_id = self.request.query_params['detail_id']
        return DetailsComments.objects.filter(detail_id=detail_id)


class DetailsCommentAddView(CreateAPIView):
    """
    头条评论添加
    """
    permission_classes = [IsAuthenticated]
    serializer_class = DetailsCommentsAddSerializer


class DetailsListView(ListAPIView):
    """
    头条列表展示
    """
    pagination_class = StandardResultsSetPagination
    serializer_class = DetailsListSerializer

    # 排序
    filter_backends = [OrderingFilter]
    ordering_fields = ('create_time', )

    def get_queryset(self, *args, **kwargs):
        cid = self.kwargs['cid']
        if cid == "0":
            return Details.objects.all()
        else:
            return Details.objects.filter(category_id=cid)


class DetailsContentView(ListAPIView):
    """
    头条详情页面展示
    """
    def get_queryset(self,*args,**kwargs):
        id = self.kwargs['id']
        return Details.objects.filter(id=id)

    serializer_class = DetailsContentSerializer


class DetailsSpecialListView(ListAPIView):
    queryset = DetailsSpecial.objects.all()[:5]
    # 选取合适的序列化器类
    serializer_class = DetailsSpecialListSerializer


class DetailsUserCollectView(CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = DetailsUserCollectSerializer


















