from django.conf.urls import url
from . import views

urlpatterns = [
    # 头条展示
    url(r'^detail/class/$',views.DetailsView.as_view()),  # 头条分类
    url(r'^detail/publish/$', views.DetailsPublishView.as_view()),# 头条发布
    url(r'^special/$', views.SaveDetailsSpecialView.as_view()),# 专栏添加
    url(r'^details/(?P<cid>\d+)/$', views.DetailsListView.as_view()),# 头条列表展示
    url(r'^detail/(?P<id>\d+)/$',views.DetailsContentView.as_view()),# 头条详情页面展示
    url(r'^special/list/$', views.DetailsSpecialListView.as_view()),  # 专栏列表展示
    url(r'^details/collect/$', views.DetailsUserCollectView.as_view()), #  用户收藏
    url(r'^details/comments/$', views.DetailsCommentsView.as_view()),
    url(r'^details/comments/add/$', views.DetailsCommentAddView.as_view()),
]

