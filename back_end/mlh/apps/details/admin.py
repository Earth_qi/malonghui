from django.contrib import admin

# Register your models here.

from . import models

admin.site.register(models.DetailsCategory)
admin.site.register(models.DetailsSpecial)
admin.site.register(models.Details)
admin.site.register(models.DetailsComments)
# admin.site.register(models.DetailsRelation)
