from rest_framework import serializers
from .models import DetailsCategory, Details, DetailsSpecial, DetailsComments

from details.models import Details, DetailsCategory
from users.models import User


class DetailsClassSerializer(serializers.ModelSerializer):
    """
    头条分类序列化器
    """
    class Meta:
        model = DetailsCategory
        # fields = '__all__'
        fields = ('id','name')


class SaveDetailsSerializer(serializers.ModelSerializer):
    """
    创建头条序列化器
    """
    class Meta:
        model = Details
         # ？？？是否需要ｉｄ进行验证
        fields = ('category', 'title', 'label', 'outline', 'user', 'detail_text')
        extra_kwargs = {
            'category':{
                'write_only':True,
                'required':True,
            },
            'title':{
                'write_only':True,
                'required':True,
            },
            'Label': {
                'write_only': True,
                'required': True,
            },
            'outline': {
                'write_only': True,
                'required': True,
            },
            'detail_text': {
                'write_only': True,
                'required': True,
            },
        }
        read_only_fields = ('user',)

    def create(self, validated_data):
        """
        保存
        """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class SaveSpecialSerializer(serializers.ModelSerializer):
    """
    添加专栏序列化器
    """
    class Meta:
        model = DetailsSpecial
        fields = ('user','title','special_url','desc')
        extra_kwargs = {
            'title': {
                'write_only': True,
                'required': True,
            },
            'special_url': {
                'write_only': True,
                'required': True,
            },
            'desc': {
                'write_only': True,
                'required': True,
            },
        }
        read_only_fields = ('user',)

    def create(self, validated_data):
        """
        保存
        """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class DetailsUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ["id", "username", "head_portrait"]


class DetailsListSerializer(serializers.ModelSerializer):

    user = DetailsUserSerializer(read_only=True)

    class Meta:
        model = Details
        fields = ["id", "user", "outline", "create_time", "title"]


class DetailsContentSerializer(serializers.ModelSerializer):
    """头条详情序列化器"""
    user = DetailsUserSerializer()

    class Meta:
        model = Details
        fields = ['id','title','detail_text','create_time','user']


class DetailsCommentsSerializer(serializers.ModelSerializer):
    """头条评论"""
    user = DetailsUserSerializer(read_only=True)

    class Meta:
        model = DetailsComments
        fields = ['id', 'parent_id', 'comment_text', "user"]


class DetailsCommentsAddSerializer(serializers.ModelSerializer):
    """
    头条评论添加
    """
    detail_id = serializers.IntegerField(required=True, label="头条详情id")

    class Meta:
        model = DetailsComments
        fields = ['parent_id', 'comment_text', "user", 'detail_id']
        read_only_fields = ('user',)

    def create(self, validated_data):
        """
        保存
        """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class DetailsSpecialListSerializer(serializers.ModelSerializer):
    """
    最新专栏的显示
    """
    user = DetailsUserSerializer(read_only=True)

    class Meta:
        model = DetailsSpecial
        fields = ['title', 'special_url', 'desc', 'user']


class DetailsUserCollectSerializer(serializers.Serializer):
    """
    收藏
    """
    id = serializers.IntegerField(required=True, label="头条详情id")

    def validate(self, attrs):
        detail_id = attrs['id']
        try:
            detail = Details.objects.get(id=detail_id)
        except Details.DoesNotExist:
            raise serializers.ValidationError('头条不存在')

        attrs['detail'] = detail

        return attrs

    def create(self, validated_data):
        detail = validated_data['detail']
        user = self.context['request'].user
        detail.collect_user.add(user)
        detail.save()

        return detail