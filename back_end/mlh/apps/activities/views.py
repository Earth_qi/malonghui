from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListAPIView, RetrieveAPIView
from .models import Activities
from rest_framework import serializers


class ActivityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activities
        fields = ["id", "title", "start_time", "status", "city", "cover"]


# 活动列表

class ActivityListView(ListAPIView):
    queryset = Activities.objects.all().order_by("-start_time")
    serializer_class = ActivityListSerializer


# 1

# 活动详情
class ActivityDetailSeralizer(serializers.ModelSerializer):
    class Meta:
        model = Activities
        exclude = ["city"]


class ActivityDetailView(RetrieveAPIView):
    queryset = Activities.objects.all()
    serializer_class = ActivityDetailSeralizer
