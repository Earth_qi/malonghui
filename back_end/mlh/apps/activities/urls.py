from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^activities/$', views.ActivityListView.as_view()),
    url(r'^activities/(?P<pk>\d+)/$',views.ActivityDetailView.as_view()),
]
