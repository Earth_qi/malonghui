from django.db import models


# Create your models here.
# 在



class Activities(models.Model):
    # id = models.IntegerField(max_length=20,verbose_name="ID")
    title = models.CharField(max_length=32, verbose_name="标题")
    start_time = models.DateTimeField(verbose_name="开始时间")
    end_time = models.DateTimeField(verbose_name="结束时间")
    city = models.CharField(max_length=64, verbose_name="城市")
    address = models.CharField(max_length=100, verbose_name="详细地址")
    desc = models.TextField(verbose_name="简介")
    sponsor = models.CharField(max_length=50, verbose_name="主办方")
    deadline = models.DateTimeField(verbose_name="报名截止时间")
    url = models.CharField(max_length=100, verbose_name="链接路径")
    status = models.SmallIntegerField(verbose_name="活动状态")
    cover = models.ImageField(max_length=100, verbose_name="图片")
    detail = models.TextField(verbose_name="详情")

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tb_activity'
        verbose_name = "活动"
        verbose_name_plural = verbose_name

class UserActivities(models.Model):
    """
    标签关注表
    """
    hd_user = models.ForeignKey("users.User", on_delete=models.CASCADE, verbose_name="用户id")
    activities = models.ForeignKey('Activities', on_delete=models.CASCADE, verbose_name='活动id')

    class Meta:
        db_table = "tb_activity_users"
        verbose_name = "活动报名表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return "%s:%s" % (self.user.username, self.tags.name)
