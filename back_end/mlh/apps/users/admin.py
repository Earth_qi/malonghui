from django.contrib import admin

from users.models import User, Address, Experience

admin.site.register(User)
admin.site.register(Address)
admin.site.register(Experience)