import re

from rest_framework import serializers
from django_redis import get_redis_connection
from rest_framework_jwt.settings import api_settings

from .models import User, Experience, Address


class UserRegisterSerializer(serializers.ModelSerializer):
    """用户注册序列化器"""

    # username password sms_code mobile allow 的校验
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='用户协议勾选', write_only=True)
    token = serializers.CharField(label='jwt_token', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'sms_code', 'mobile', 'allow', 'token']
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validated(self, attrs):
        """
        完成注册信息的校验
        """

        # username password sms_code mobile allow 的校验
        mobile = attrs['mobile']
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            raise serializers.ValidationError('手机号格式错误')

        allow = attrs['allow']
        if allow != 'true':
            raise serializers.ValidationError('未同意用户协议')

        sms_code = attrs['sms_code']
        redis_conn = get_redis_connection("verify_codes")
        # 取出值为byte类型
        real_sms_code = redis_conn.get("sms_%s" % mobile)
        if not real_sms_code:
            raise serializers.ValidationError('短信验证码无效')
        if sms_code != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')

        return attrs

    def create(self, validated_data):
        """创建用户资料"""

        # 删除需要的参数
        validated_data.pop('sms_code')
        validated_data.pop('allow')
        user = super().create(validated_data)

        user.set_password(validated_data['password'])
        user.save()

        # jwt认证
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token

        return user


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['name', ]


class ExperienceSerializer(serializers.ModelSerializer):
    experience_address = AddressSerializer()
    class Meta:
        model = Experience
        exclude = ('create_time', 'update_time', 'type', 'user')


class UserFileRetrieveUpdateSerializer(serializers.ModelSerializer):
    educations = ExperienceSerializer(many=True)
    works = ExperienceSerializer(many=True)
    gender = serializers.CharField(source='get_gender_display')

    # address = AddressSerializer(Address.objects.filter('is_now'))

    class Meta:
        model = User
        fields = ['username', 'mobile', 'gender', 'head_portrait', 'personal_site', 'real_name', 'birth_date',
                  'addresses', 'email', 'works', 'educations', 'short_description']
        # fields = '__all__'
        depth = 1


class UserBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = (
        "last_login", "is_superuser", "first_name", "last_name", "is_staff", "is_active", "date_joined", "groups",
        "user_permissions")
