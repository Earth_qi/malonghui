from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    # 用户名称判断
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UserNameView.as_view()),
    # 手机号重复判断
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileView.as_view()),
    # 发送短信
    url(r'^sms_code/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),
    # 用户注册
    url(r'^users/$', views.UserRegisterView.as_view()),
    # jwt登录认证
    url(r'^authorizations/$', obtain_jwt_token),
    url(r'^users/my_files/(?P<pk>\d+)/$', views.UserFileView.as_view()),
    url(r'^users/my_file/(?P<pk>\d+)/$', views.UserBasicView.as_view()),
    url(r'^users/my_experience/(?P<pk>\d+)/$', views.ExperienceView.as_view()),
]
