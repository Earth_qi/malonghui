from django.db import models
from django.contrib.auth.models import AbstractUser

from areas.models import Area
from mlh.utils.models import BaseModel


class User(AbstractUser):
    """用户模型类"""

    USER_GENDER_CHOICES = (
        (1, '男'),
        (2, '女'),
    )
    mobile = models.CharField(max_length=11, unique=True, default="暂无", verbose_name='手机号')
    gender = models.SmallIntegerField(choices=USER_GENDER_CHOICES, default=1, verbose_name="性别")
    head_portrait = models.FileField(max_length=50, default="", null=True, blank=True, verbose_name="头像")
    personal_site = models.CharField(max_length=64, default="暂无", null=True, blank=True, verbose_name="个人网址")
    real_name = models.CharField(max_length=32, default="暂无", verbose_name="真实姓名")
    birth_date = models.DateField(default="1997-7-1", verbose_name="出生日期")
    short_description = models.TextField(max_length=256, default="这个人很懒，什么都没有写", null=True, verbose_name="个人简介")


    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name


class UserToUser(BaseModel):
    """用户关注用户"""
    usertuser = models.ForeignKey(User, related_name="tuser")
    userfuser = models.ForeignKey(User, related_name="fuser")
    class Meta:
        db_table = 'tb_user_to_user'
        verbose_name = '用户关注'
        verbose_name_plural = verbose_name


class Address(BaseModel):
    """
    用户地址
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='addresses', verbose_name='用户')
    name = models.CharField(max_length=50, verbose_name='地址名称')
    province = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='province_addresses', verbose_name='省')
    city = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='city_addresses', verbose_name='市')
    district = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='district_addresses', verbose_name='区')
    is_now = models.BooleanField(default=False, verbose_name="是否是现住址")

    class Meta:
        db_table = 'tb_address'
        verbose_name = '用户地址'
        verbose_name_plural = verbose_name
        ordering = ['-update_time']  # 指明默认排序

    def __str__(self):
        return self.name


class Experience(BaseModel):
    USER_EXPERIENCE_CHOICES = (
        (1, '工作经历'),
        (2, '教育经历'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')
    experience_address = models.ForeignKey(Address, on_delete=models.CASCADE, verbose_name="地址")
    experience_content = models.CharField(max_length=32, verbose_name="工作/教育经历")
    start_time = models.DateField(verbose_name="开始时间")
    end_time = models.DateField(verbose_name="结束时间")
    type = models.SmallIntegerField(choices=USER_EXPERIENCE_CHOICES, default=1, verbose_name="类型")
    skills = models.CharField(max_length=64, null=True, verbose_name="技术")
    description = models.CharField(max_length=256, null=True, verbose_name="描述")

    class Meta:
        db_table = 'tb_experience'
        verbose_name = '用户教育/工作经历'
        verbose_name_plural = verbose_name
        ordering = ['-update_time']  # 指明默认排序


class OAuthUser(BaseModel):
    """
    用户绑定数据
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户')
    we_chat_openid = models.CharField(max_length=64, verbose_name='we_chat_openid', null=True, db_index=True)
    qq_openid = models.CharField(max_length=64, verbose_name='qq_openid', null=True, db_index=True)

    class Meta:
        db_table = 'tb_oauth'
        verbose_name = '用户绑定数据'
        verbose_name_plural = verbose_name