from django.conf.urls import url

from . import views

urlpatterns = [
    # 问答模块
    url(r'^questions/(?P<pk>\d+)/$', views.QuestionDetailView.as_view()),
    # 问答首页
    url(r'^questions/index/$', views.QuestionIndexView.as_view()),
    # 所有标签
    url(r'^questions/tags/$', views.TagsView.as_view()),
    # 回答点赞
    url(r'^questions/(?P<pk>\d+)/likes/$', views.AnswerLikesView.as_view()),
    # 问题提交
    url(r'^questions/submission/$', views.QuestionSubmitView.as_view()),
]
