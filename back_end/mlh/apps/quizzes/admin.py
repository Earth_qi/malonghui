from django.contrib import admin

from . import models

admin.site.register(models.UserTags)
admin.site.register(models.Tags)
admin.site.register(models.QuestionTag)
admin.site.register(models.Question)
admin.site.register(models.Answer)


