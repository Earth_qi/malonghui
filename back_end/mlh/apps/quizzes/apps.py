from django.apps import AppConfig


class QuizzesConfig(AppConfig):
    name = 'quizzes'
    verbose_name = "问答表"
