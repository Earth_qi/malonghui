"""mlh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from ckeditor_uploader.views import ImageUploadView
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^ckeditor/', include('ckeditor_uploader.urls')),  # 为富文本编辑器添加总路由
    # url(r'^ckeditor/', include('ckeditor_uploader.urls')),  # 为富文本编辑器添加总路由
    # url(r'^ckeditor/upload/', ImageUploadView.as_view()),  # 为富文本编辑器添加总路由
    # url(r'^ckeditor/upload/', csrf_exempt(ImageUploadView.as_view())),  # 为富文本编辑器添加总路由
    url(r'^ckeditor/', csrf_exempt(ImageUploadView.as_view())),  # 为富文本编辑器添加总路由

    url(r'', include('users.urls')),

    # url(r'', include('recruit.urls')),
    url(r'', include('tsukkomis.urls')), # 吐槽模块
    url(r'', include('activities.urls')),
    # 头条
    url(r'', include('details.urls')),
    # 问答模块
    url(r'', include("quizzes.urls")),
    url(r'', include("areas.urls")),

# ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
]
# 1/home/python/.virtualenvs/django_1.11.16_py3/lib/python3.5/site-packages/ckeditor/static/ckeditor/ckeditor/ckeditor.js