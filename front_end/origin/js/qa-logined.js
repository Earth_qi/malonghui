let vm = new Vue({
    el: "#app",
    data: {
        host: host,
        // 后期用户登录
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        // 相关数据
        question_list: [],
        page: 1,
        page_size: 5,
        ordering: '-create_time',
        answer_count: '', // 等待回答
        url: '',
        flag: true,
        count: 0
    },
    computed: {
        total_page: function () {  // 总页数
            return Math.ceil(this.count / this.page_size);
        },
        next: function () {  // 下一页
            if (this.page >= this.total_page) {
                return 0;
            } else {
                return this.page + 1;
            }
        },
        previous: function () {  // 上一页
            if (this.page <= 0) {
                return 0;
            } else {
                return this.page - 1;
            }
        },
        page_nums: function () {  // 页码
            // 分页页数显示计算
            // 1.如果总页数<=5
            // 2.如果当前页是前3页
            // 3.如果当前页是后3页,
            // 4.既不是前3页，也不是后3页
            let nums = [];
            if (this.total_page <= 5) {
                for (let i = 1; i <= this.total_page; i++) {
                    nums.push(i);
                }
            } else if (this.page <= 3) {
                nums = [1, 2, 3, 4, 5];
            } else if (this.total_page - this.page <= 2) {
                for (let i = this.total_page; i > this.total_page - 5; i--) {
                    nums.push(i);
                }
            } else {
                for (let i = this.page - 2; i < this.page + 3; i++) {
                    nums.push(i);
                }
            }
            return nums;
        }
    },
    mounted: function () {
        // 如果登录的话就转到登录页面
        if (!this.user_id && !this.token) {
            window.location.replace('./qa-login.html')
        }
        this.flush_page()
    },
    methods: {
        flush_page: function () {
            if (this.flag) {
                this.url = this.host + '/questions/index/?page=' + this.page + '&page_size=' + this.page_size + '&ordering=' + this.ordering
            } else {
                this.url = this.host + '/questions/index/?Fpage=' + this.page + '&page_size=' + this.page_size + '&ordering=' + this.ordering + this.answer_count
            }

            axios.get(this.url, {
                responseType: "json"
            })
                .then(response => {
                    this.count = response.data.count;
                    this.question_list = response.data.results
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        },
        // 时间处理
        handle_time: function (create_time) {
            create_time = new Date(create_time);
            let year = create_time.getFullYear();
            let month = create_time.getMonth() + 1;
            let day = create_time.getDate();
            let hour = create_time.getHours();
            let minute = create_time.getMinutes();
            let time_message = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
            return time_message
        },
        // 热门回答
        popular_answer: function (e) {
            this.ordering = '-likes_count';
            this.flag = true;
            e.currentTarget.parentElement.setAttribute("class", "active");
            e.currentTarget.parentElement.previousElementSibling.setAttribute("class", "");
            e.currentTarget.parentElement.nextElementSibling.setAttribute("class", "");
            this.flush_page()
        },
        // 等待回答
        waiting_answer: function (e) {
            this.answer_count = '&answer_count=0';
            this.flag = false;
            e.currentTarget.parentElement.setAttribute("class", "active");
            e.currentTarget.parentElement.previousElementSibling.setAttribute("class", "");
            e.currentTarget.parentElement.previousElementSibling.previousElementSibling.setAttribute("class", "");
            this.flush_page()
        },
        // 最新回答
        lasted_answer: function (e) {
            this.ordering = '-create_time';
            this.flag = true;
            e.currentTarget.parentElement.setAttribute("class", "active");
            e.currentTarget.parentElement.nextElementSibling.setAttribute("class", "");
            e.currentTarget.parentElement.nextElementSibling.nextElementSibling.setAttribute("class", "");
            this.flush_page()
        },
        // 点击页数
        on_page: function (num) {
            if (num != this.page) {
                this.page = num;
                this.flush_page();
            }
        },
        // 点击排序
        on_sort: function (ordering) {
            if (ordering != this.ordering) {
                this.page = 1;
                this.ordering = ordering;
                this.flush_page();
            }
        },
    }
});