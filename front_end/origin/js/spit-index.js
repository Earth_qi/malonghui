/**
 * Created by python on 18-12-11.
 */
var vm = new Vue({
    el: "#app",
    data: {
        page: 1,
        page_size: 5,
        count: 0,
        url:'',
        // 页面中需要使用到的数据，键值对
        tsukkomis: {},
    },
    computed: {
        total_page: function () {  // 总页数
            return Math.ceil(this.count / this.page_size);
        },
        next: function () {  // 下一页
            if (this.page >= this.total_page) {
                return 0;
            } else {
                return this.page + 1;
            }
        },
        previous: function () {  // 上一页
            if (this.page <= 0) {
                return 0;
            } else {
                return this.page - 1;
            }
        },
        page_nums: function () {  // 页码
            // 分页页数显示计算
            // 1.如果总页数<=5
            // 2.如果当前页是前3页
            // 3.如果当前页是后3页,
            // 4.既不是前3页，也不是后3页
            var nums = [];
            if (this.total_page <= 5) {
                for (var i = 1; i <= this.total_page; i++) {
                    nums.push(i);
                }
            } else if (this.page <= 3) {
                nums = [1, 2, 3, 4, 5];
            } else if (this.total_page - this.page <= 2) {
                for (var i = this.total_page; i > this.total_page - 5; i--) {
                    nums.push(i);
                }
            } else {
                for (var i = this.page - 2; i < this.page + 3; i++) {
                    nums.push(i);
                }
            }
            return nums;
        }
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        this.get_page();

    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        get_page: function () {
            axios.get(host + "/tsukkomis/", {
                params: {
                    page: this.page,
                    page_size: this.page_size,
                },
                responseType: 'json'
            })
                .then(response => {
                    this.count = response.data.count;
                    this.tsukkomis = response.data.results;

                })

        },
        // 点击页数
        on_page: function (num) {
            if (num != this.page) {
                this.page = num;
                this.get_page();
            }
        },
    }
});


