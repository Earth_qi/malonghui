var vm = new Vue({
    el: '#app',
    data: {
        host: host,
        // 注册用变量
        register_username: '',
        register_mobile: '',
        register_pwd: '',
        register_smscode: '',
        smscode_tips: '获取验证码',
        register_allow: false,
        register_allow_flag: false,
        error_register_username: '',
        smscode_sending_flag: false,
        error_register_username_flag: false,
        error_register_mobile: '',
        error_register_mobile_flag: false,
        error_register_pwd: '',
        error_register_pwd_flag: false,
        error_register_smscode: '',
        error_register_smscode_flag: false
    },
    methods: {
        // 获取url路径参数
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        // 要与登录的区分开
        check_register_username: function () {
            if (!this.register_username) {
                this.error_register_username = '用户名不能为空';
                this.error_register_username_flag = true;
                return
            }
            let len = this.register_username.length;
            if (len < 5 || len > 20) {
                this.error_register_username = '请输入长度为5-20位的用户名';
                this.error_register_username_flag = true;
                return
            }
            this.error_register_username_flag = false;
            axios.get(this.host + '/usernames/' + this.register_username + '/count/', {
                responseType: 'json'
            })
                .then(response => {
                    if (response.data.count > 0) {
                        this.error_register_username = '用户名已被注册';
                        this.error_register_username_flag = true;
                    }
                    else {
                        this.error_register_username_flag = false;
                    }
                })
                .catch(error => {
                        console.log(error.response.data);
                    }
                )
        },
        check_register_mobile: function () {
            if (!this.register_mobile) {
                this.error_register_mobile = '手机号不能为空';
                this.error_register_mobile_flag = true;
                return
            }
            let re = /^1[345789]\d{9}$/;
            if (!re.test(this.register_mobile)) {
                this.error_register_mobile = '您输入的手机号格式不正确';
                this.error_register_mobile_flag = true;
                return
            }
            this.error_register_mobile_flag = false;
            axios.get(this.host + '/mobiles/' + this.register_mobile + '/count/', {
                responseType: 'json'
            })
                .then(response => {
                    if (response.data.count > 0) {
                        this.error_register_mobile = '手机号已被注册';
                        this.error_register_mobile_flag = true;
                    }
                    else {
                        this.error_register_mobile_flag = false;
                    }
                })
                .catch(error => {
                        console.log(error.response.data);
                    }
                )
        },
        check_register_pwd: function () {
            if (!this.register_pwd) {
                this.error_register_pwd = '密码不能为空';
                this.error_register_pwd_flag = true;
                return
            }
            let len = this.register_pwd.length;
            if (len < 6 || len > 16) {
                this.error_register_pwd = '请输入长度为6-16位的密码';
                this.error_register_pwd_flag = true;
                return
            }
            this.error_register_pwd_flag = false;
        },
        check_register_allow: function () {
            if (!this.register_allow) {
                this.register_allow_flag = true;
                return
            }
            this.register_allow_flag = false;
        },
        check_smscode: function () {
            if (!this.register_smscode) {
                this.error_register_smscode = '短信验证码不能为空';
                this.error_register_smscode_flag = true;
                return
            }
            this.error_register_smscode_flag = false;
        },
        send_sms: function () {
            if (this.smscode_sending_flag == true) {
                // 如果当前已点发送短信了，就不再重复请求后端
                return
            }
            this.smscode_sending_flag = true;

            // 校验参数
            this.check_register_username();
            this.check_register_mobile();

            if (this.error_register_mobile_flag == true || this.error_register_username_flag == true) {
                this.smscode_sending_flag = false;
                alert('请检查用户名或手机是否正确');
                return
            }
            // 向后端接口请求
            axios.get(this.host + '/sms_code/' + this.register_mobile + '/', {
                responseType: 'json'
            })
                .then(response => {
                    // 短信发送成功后，就进行60s的倒计时
                    // 定义一个60s的变量
                    let count = 60;
                    // 设置定时器
                    let count_timer = setInterval(() => {
                            if (count == 1) {
                                // 清除定时器
                                clearInterval(count_timer);
                                // 恢复文字内容
                                this.smscode_tips = '获取验证码';
                                // 恢复点击事件
                                this.smscode_sending_flag = false;
                            } else {
                                count -= 1;
                                this.smscode_tips = count + '秒';
                            }
                        }
                        , 1000, 60)
                })
                .catch(error => {
                    if (error.response.status == 400) {
                        console.log(error.response.data)
                    }
                })
        },
        on_submit: function () {
            // 校验参数
            this.check_register_username();
            this.check_register_mobile();
            this.check_register_allow();
            this.check_smscode();
            this.check_register_pwd();
            if (this.error_register_smscode_flag == false && this.error_register_pwd_flag == false && this.error_register_username_flag == false &&
                this.register_allow_flag == false && this.error_register_mobile_flag == false) {
                axios.post(this.host + '/users/', {
                    username: this.register_username,
                    password: this.register_pwd,
                    sms_code: this.register_smscode,
                    mobile: this.register_mobile,
                    allow: this.register_allow.toString()
                }, {responseType: 'json'})
                    .then(response => {
                        sessionStorage.clear();
                        localStorage.clear();

                        localStorage.token = response.data.token;
                        localStorage.username = response.data.username;
                        localStorage.user_id = response.data.id;
                        location.href = '/index.html';
                    })
                    .catch(error => {
                        if (error.response.status == 400) {
                            alert('注册失败')
                        }
                    })
            } else {
                alert('参数不完整')
            }
        }
    }
});