var vm = new Vue({
    el: '#app',
    data: {
        host: host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        comments_list: [],
        c_list: [],
        message: "",
        detail_content:[],
        title:"",
        detail_text:"",
        create_time:"",
        user:'',
        // id:"",
        display: 'none',

    },
    mounted: function(){
        this.get_comments_list();
        this.get_detail_content();
    },
    methods: {
        get_comments_list: function() {
            var detail_id = this.get_query_string('detail_id');
            // 头条评论展示
            axios.get(this.host+'/details/comments/?detail_id='+ detail_id, {
                responseType:'json'
            })
            .then(response => {
                // 头条评论数据
                this.comments_list = response.data;
                for (i = 0; i < this.comments_list.length; i++){
                    if (this.comments_list[i].parent_id == -1){
                        this.c_list.push({
                            id: this.comments_list[i].id,
                            comment_text: this.comments_list[i].comment_text,
                            user: this.comments_list[i].user,
                            children: []
                        });
                        for (j = 0; j < this.comments_list.length; j++){
                            com = this.c_list[this.c_list.length-1];
                            if (this.comments_list[j].parent_id == this.comments_list[i].id) {
                                com.children.push({
                                    id: this.comments_list[j].id,
                                    comment_text: this.comments_list[j].comment_text,
                                    user: this.comments_list[j].user
                                })
                            }
                        }
                    }
                }
            })
            .catch(error => {
                console.log(error.response.data)
            });
        },
        get_detail_content:function () {
            var detail_id = this.get_query_string('detail_id');
            axios.get(this.host+'/detail/'+ detail_id, {
                responseType:'json'
            })
            .then(response=> {
                //头条详情页内容
                this.title = response.data[0].title;
                this.detail_text = response.data[0].detail_text;
                this.create_time = response.data[0].create_time;
                this.user = response.data[0].user;
            })
            .catch(error =>{
                alert('页面加载失败')

            })
        },
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        show: function(index) {
            var display = this.$refs[index][0].style.display;
            if (display == "block") {
                this.$refs[index][0].style.display = "none";
            }else {
                this.$refs[index][0].style.display = "block";
            }
        },
        submit: function (index) {
            var detail = this.get_query_string('detail_id');
            axios.post(this.host+'/details/comments/add/', {
                detail_id: detail,
                comment_text: this.message,
                parent_id: index,
            }, {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
            })
            .then(response => {
                // 头条列表数据
                if (index != -1){
                    this.$refs[index][0].style.display = "none";
                }
                this.message = "";
                this.comments_list = [];
                this.c_list = [];
                this.get_comments_list();
            })
            .catch(error => {
                console.log(error.response.data)
            });

        },
        onSubmit(){return false;},
        enter(){
            this.display = 'block';
        },
        leave(){
            this.display = 'none';
        },
        collect: function() {
            var detail = this.get_query_string('detail_id');
            axios.post(this.host+'/details/collect/', {
                id: detail,
            }, {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
            })
            .then(response => {
                alert(123)
            })
            .catch(error => {
                console.log(error.response.data)
            });
        }

    }
});