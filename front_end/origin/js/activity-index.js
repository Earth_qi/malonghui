/**
 * Created by python on 18-12-10.
 */
let app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        activities:[]
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        axios.get(host + '/activities/')
            .then(response => {
                // console.log(response.data)
                this.activities = response.data
            })
            .catch(error => {
            })
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})

