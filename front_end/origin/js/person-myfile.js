var vm = new Vue({
    el: '#app',
    data: {
        host: host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        file: '',
        is_show: true,
        is_show_form: false,
        show_work: true,
        show_work_form: false,
        show_description: true,
        show_description_form: false,
        show_info: true,
        show_info_form: false,
        show_skills: true,
        show_skill_form: false,
        // 页面空数据判断
        addresses: '',
        education: '',
        works: '',
        experience: {},
        tags: '',
        tag_id: 0
    },
    mounted: function(){
        if (this.user_id && this.token) {
            axios.get(this.host+'/users/my_files/'+ this.user_id, {
                responseType: 'json'
                })
                .then(response => {
                    this.file = response.data;
                    this.addresses = this.file.addresses
                    // console.log(response.data)
                    // console.log(this.file)
                    })
                .catch(error => {
                    console.log(error.response.data);
                })
        } else {
            location.href = '/person-loginsign.html'
        }

    },
    methods: {
        // 获取url路径参数
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        // 添加工作经历
        addWorkExperience: function(){
            this.show_work = false;
            this.show_work_form = true
        },
        // 添加个人简介
        addShortDescription: function(){
            this.show_description= false;
            this.show_description_form= true
        },
        saveDescription: function(){
            this.show_description= false;
            this.show_description_form= true
        },
        // 编辑个人信息
        editPersonalInfo: function(){
            this.show_info = false;
            this.show_info_form = true
        },
        // 编辑教育经历
        addEducationExperience: function(){
            this.is_show = false;
            this.is_show_form = true
        },
        // 修改技能
        modifySkill:function(){
            this.show_skills = false;
            this.show_skill_form = true
        },
        // 添加个人技能标签
        add_tags: function () {
            axios.patch(this.host+'/users/my_experience/' + this.id,{"tags":this.tags},{
                // name=&birthday=&telphone=&city=&qudao=on&net=&email=&city=
                headers: {
                    "Authorization": "JWT " + this.token
                },
                responseType: "json"
            })
                .then(response => {

                })
                .catch(error => {

                })
        },
        // 获取当前标签的id
        get_experience_id: function (id) {
            this.experience_id = id
        },
        // 修改个人信息
        modify_info: function () {
            axios.patch(this.host+'/users/my_file/' + this.id,{
                // name=&birthday=&telphone=&city=&qudao=on&net=&email=&city=
                headers: {
                    "Authorization": "JWT " + this.token
                },
                responseType: "json"
            })
                .then(response => {

                })
                .catch(error => {

                })
        },
        // get_experience: function(){
        //     if(this.tags){
        //         this.experience['skills'] = this.tags
        //     }elseif(this.){}
        // }
    },
});