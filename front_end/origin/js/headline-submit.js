var vm = new Vue({
    el: '#app',
    data:{
        host:host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        isShowCategory: false,
        isShowTag: false,
        title: "",
        class_list: [],
        class_name: "请选择分类",
        class_id: null,
        content: "",
        tag_list: ['javascript', 'php', 'css', 'html', 'java', 'python', 'html5', 'node.js', 'c++'],
        tag_show_list: [],
        tag_text: "",
    },
    mounted:function () {
        CKEDITOR.replace('editor1');

        // 请求分类
        axios.get(this.host+'/detail/class/', {
            responseType:'json'
        })
        .then(response => {
            // 获取分类数据
            this.class_list = response.data;
            this.class_list.splice(0,1);
        })
        .catch(error => {
            // console.log(error.response.data)
        });
        this.tag_background = []
    },
    methods:{
        onsubmit:function () {
            alert(123);
            this.content = CKEDITOR.instances.editor_text.getData();
            var outline = this.content.substr(0,10);
            axios.post(host + '/detail/publish/',{
                title: this.title,
                category: this.class_id,
                label: this.tag_text,
                detail_text: this.content,
                outline: outline,
            },{
                headers:{
                    "Authorization": "JWT " + this.token
                },
                responseType:"json"
            })
                .then(response=>{
                    alert("提交成功");
                    window.location.reload() // 刷新页面
                })
                .catch(error =>{})
        },
        show_category: function () {
            this.isShowCategory = !this.isShowCategory
        },
        select_category: function (name, id) {
            this.class_name = name;
            this.isShowCategory = false;
            this.class_id = id;
        },
        show_tag: function() {
            this.isShowTag = !this.isShowTag
        },
        select_tag: function (index) {
            if (this.tag_show_list.indexOf(this.tag_list[index]) === -1){
                document.getElementById("tag"+index).style.background = "red";

                this.tag_show_list.push(this.tag_list[index]);
                this.tag_text = "";
                for (i=0; i < this.tag_show_list.length; i++){
                    this.tag_text += (this.tag_show_list[i] + ";  ");
                }
            } else {
                document.getElementById("tag"+index).style.background = "#e1ecf4";

                var tag_index = this.tag_show_list.indexOf(this.tag_list[index]);
                this.tag_show_list.splice(tag_index,1);
                this.tag_text = "";
                for (i=0; i < this.tag_show_list.length; i++){
                    this.tag_text += (this.tag_show_list[i] + ";  ");
                }
            }
        },
        focus_textarea: function () {
            this.isShowTag = false;
        },
    }
});
