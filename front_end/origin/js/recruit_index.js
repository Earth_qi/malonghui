// let vm = new Vue({
//     el: '#app',
//     data: {
//         host: host,
//         username: sessionStorage.username || localStorage.username,
//         user_id: sessionStorage.user_id || localStorage.user_id,
//         token: sessionStorage.token || localStorage.token,
//         new_jobs: [], // 最新职位
//         recommended_jobs: [], // 推荐职位
//         no_data: true,
//         now:0,
//         deadline:0,
//         time2now:0,
//         time2now_list:[],
//         enterprises:[],
//         no_enter: true
//     },
//     mounted: function(){
//         this.get_recommended_jobs();
//         this.get_hot_enterprise();
//         axios.get(this.host+'/recruit_index/', {
//                 params: {
//                     px: 'new'
//                 },
//                 responseType: 'json'
//                 })
//                 .then(response => {
//
//                     this.new_jobs = response.data;
//
//                     this.now = new Date(response.headers.date).getTime();
//
//                     for(let i=0; i<this.new_jobs.length; i++){
//                         this.new_jobs[i].url = this.host + '/recruit_job/' + this.new_jobs[i].id;
//
//                         this.deadline = new Date(this.new_jobs[i].create_time).getTime();
//                         console.log(this.deadline)
//
//                         this.time2now = this.now - this.deadline;
//                         this.time2now = Math.floor(this.time2now/(1000*60*60*24))
//                         this.new_jobs[i]["time2now"]=this.time2now
//                         console.log(this.time2now)
//
//
//                     };
//
//                 })
//                 .catch(error => {
//                     console.log(error.response.data);
//                 })
//     },
//     methods:{
//        get_recommended_jobs: function(){
//            axios.get(this.host+'/recruit_index/recommended/', {
//                responseType: 'json'
//            })
//                .then(response => {
//                    if(response.data.data!="no_data"){
//                         this.no_data = true;
//                         this.recommended_jobs = response.data;
//                         this.now = new Date(response.headers.date).getTime();
//                         for(let i=0; i<this.recommended_jobs.length; i++){
//                         this.recommended_jobs[i].url = this.host + '/recruit_job/' + this.recommended_jobs[i].id;
//
//                         this.deadline = new Date(this.recommended_jobs[i].create_time).getTime();
//                         // console.log(this.deadline)
//
//                         this.time2now = this.now - this.deadline;
//                         this.time2now = Math.floor(this.time2now/(1000*60*60*24))
//                         this.recommended_jobs[i]["time2now"]=this.time2now
//                         // console.log(this.time2now)
//                     };
//
//                    }
//                    else{
//                        this.no_data = false
//                    }
//                })
//                .catch(error =>{
//                    console.log(error.response.data);
//                })
//        },
//        get_hot_enterprise: function(){
//            axios.get(this.host + '/recruit_index/enterprises',{
//                response_type: 'json'
//            })
//                .then(response => {
//                    if(response.data.message!="no_data") {
//                        this.no_enter = true
//                        this.enterprises = response.data;
//                         console.log(this.enterprises)
//                        for (let i = 0; i < this.enterprises.length; i++) {
//                            this.enterprises[i].url = this.host + 'recruit_company/' + this.enterprises[i].id;
//                        }
//                    }
//                    else{
//                        this.no_enter = false
//                    }
//                })
//                .catch(error=>{
//                    console.log(error.response.data);
//                })
//        }
//     }
// });