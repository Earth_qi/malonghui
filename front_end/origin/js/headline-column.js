var vm = new Vue({
    el: '#app',
    data: {
        host:host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        title: "",
        special_url: "",
        desc: "",
    },
    mounted: function(){

    },
    methods: {
        bu_onsubmit: function () {
            axios.post(this.host+'/special/', {
                title: this.title,
                special_url: this.special_url,
                desc: this.desc,
            }, {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
            })
            .then(response => {
                alert("提交成功");
                window.location.reload() // 刷新页面
            })
            .catch(error => {
                console.log(error.response.data)
            });
        }
    }
});