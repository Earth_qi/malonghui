let vm = new Vue({
    el: "#app",
    data: {
        host: host,
        // 后期用户登录
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        // 问题变量参数
        questions: {},
        answers: [],
        qs_create: 0,  // 问题创建时间
        qs_update: [],   // 问题更新时间
        diaglog_show_flag: false,
        click_status: true,
        click_status_ans: true,
        answer_content: ""
    },
    mounted: function () {
        this.flash_page();
    },
    methods: {
        flash_page: function () {
            let id = this.get_query_string("id");
            axios.get(this.host + '/questions/' + id + '/', {
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
                .then(response => {
                    this.questions = response.data.question;
                    this.answers = response.data.answer;
                    let server_time_string = response.headers.date;
                    let create_time_string = this.questions.create_time;
                    let update_time_string = this.questions.update_time;
                    this.qs_create = this.question_create_time(server_time_string, create_time_string);
                    this.qs_update = this.question_update_time(server_time_string, update_time_string);
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        },
        // 获取url路径参数
        get_query_string: function (name) {
            let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            let r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        question_create_time: function (server_time, create_time) {
            server_time = new Date(server_time).getTime();
            create_time = new Date(create_time).getTime();
            // 向下取整
            qs_hours = Math.floor((server_time - create_time) / 3600000);
            return qs_hours
        },
        question_update_time: function (server_time, update_time) {
            server_time = new Date(server_time).getTime();
            update_time = new Date(update_time).getTime();
            // 向下取整
            gaps = server_time - update_time;
            update_hours = Math.floor(gaps / 3600000);
            update_minutes = Math.floor(gaps / 60000) % 60;
            return [update_hours, update_minutes]
        },
        // 问题点赞
        question_like: function (question_id) {
            // {"qs_like": 1}
            let id = this.get_query_string("id");
            axios.put(this.host + '/questions/' + id + '/?question_id=' + question_id, {"qs_like": 1}, {
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
                .then(response => {
                    // 将最新的点赞数据显示
                    this.flash_page();
                    console.log(response.data)
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        },
        question_unlike: function (question_id) {
            // {"qs_like": -1}
            let id = this.get_query_string("id");
            axios.put(this.host + '/questions/' + id + '/?question_id=' + question_id, {"qs_like": -1}, {
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
                .then(response => {
                    // 将最新的点赞数据显示
                    this.flash_page();
                    console.log(response.data)
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        },
        // 回答点赞
        answer_like: function (ans_id) {
            let id = this.get_query_string("id");
            axios.put(this.host + '/questions/' + id + '/likes/?answer_id=' + ans_id, {"qs_like": 1, "answer_id": ans_id}, {
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
                .then(response => {
                    // 将最新的点赞数据显示
                    this.flash_page();
                    console.log(response.data)
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        },
        answer_unlike: function (ans_id) {
            let id = this.get_query_string("id");
            axios.put(this.host + '/questions/' + id + '/likes/?answer_id=' + ans_id, {"qs_like": -1, "answer_id": ans_id}, {
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
                .then(response => {
                    // 将最新的点赞数据显示
                    this.flash_page();
                    console.log(response.data)
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        },
        // 显示评论的表单框
        question_dialog_show: function () {
            if (this.click_status) {
                this.diaglog_show_flag = 'block';
                this.click_status = false;
            } else {
                this.diaglog_show_flag = 'none';
                this.click_status = true;
            }
        },
        answer_dialog_show: function (e) {
            // 获取DOM节点并设置内容
            let comment_box = e.currentTarget.parentElement.parentElement.nextElementSibling;
            comment_box.setAttribute("style", "display:block")
        },
        // 问题回答提交
        answer_submit: function () {
            // 获取文本框中的内容
            // console.log(this.answer_content);
            let id = this.get_query_string("id");
            axios.post(this.host + '/questions/' + id + '/', {"content": this.answer_content}, {
                headers: {'Authorization': 'JWT ' + this.token},
                responseType: 'json'
            })
                .then(response => {
                    window.location.reload();
                })
                .catch(error => {
                    console.log(error.response.data)
                })
        }
    }
});
