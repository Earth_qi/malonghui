/**
 * Created by python on 18-12-12.
 */

let detail = new Vue({
    el: '#app',
    data: {
        tsukkomis:{},
        comments:[],
        username:'',
        tstsukkomi_id:''
    },
    computed: {},
    mounted: function () {
        this.tstsukkomi_id = this.get_query_string("tsukkomi_id");
        axios.get(host + "/tsukkomis/"+this.tstsukkomi_id+"/" , {
            pramas: {}
        })
            .then(response => {
                this.tsukkomis = response.data;
                this.comments = this.tsukkomis.comment;
                this.username = this.tsukkomis.user.username


            }).catch(error => {

        });
    },
    methods: {
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
    }


});





