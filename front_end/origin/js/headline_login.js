var vm = new Vue({
    el: '#app',
    data: {
        onFetching: false, //防止页面滑动时多次加载
        page: 0,           //滑动次数or加载数据的页数
        cat: 0,
        detail_list: [],
        page_max: 0,
        page_size: 12,
        host: host,
        class_list: [],
        special_list: [],
        current: 0,
        display: 'none',
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
    },
    mounted: function(){
        this.cat = 0;
        //axios请求和数据赋值
        // 头条列表展示
        axios.get(this.host+'/details/'+this.cat+'/?page_size='+ this.page_size, {
            responseType:'json'
        })
        .then(response => {
            // 头条列表数据
            this.detail_list = response.data.results;
            this.page_max = response.data.count/(this.page_size)
        })
        .catch(error => {
            console.log(error.response.data)
        });

        // 分类
        axios.get(this.host+'/detail/class/', {
            responseType:'json'
        })
        .then(response => {
            // 获取分类数据
            this.class_list = response.data;
        })
        .catch(error => {
            console.log(error.response.data)
        });

        // 最新专栏
        axios.get(this.host+'/special/list/', {
            responseType:'json'
        })
        .then(response => {
            // 获取分类数据
            this.special_list = response.data;
        })
        .catch(error => {
            console.log(error.response.data)
        });

        document.addEventListener('scroll', this.scrollLoad)
    },
    methods: {
        loadlist () {
            //axios请求和数据赋值
            // 头条列表展示
            axios.get(this.host+'/details/'+this.cat+'/?page_size='+ this.page_size+"&page="+ this.page, {
                responseType:'json'
            })
            .then(response => {
                // 头条列表数据
                this.detail_list = this.detail_list.concat(response.data.results);
                this.page_max = response.data.count/(this.page_size)
            })
            .catch(error => {
                // console.log(error.response.data)
            });

        },
        scrollLoad() {
            let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight; //document的滚动高度
            let nowScotop = document.documentElement.clientHeight || document.body.clientHeight;  //可视区高度
            let wheight = document.documentElement.scrollTop || document.body.scrollTop; //已滚动高度


            if (this.onFetching || this.page >= this.page_max) {
                // do nothing
            } else {
                if (nowScotop >= scrollHeight - wheight - 5) {
                    this.onFetching = true;
                    setTimeout(() => {

                        this.page += 1;
                        this.loadlist(); //加载列表的请求方法
                        this.onFetching = false
                    }, 500)
                }
            }
        },
        sub(cat, index) {
            this.cat = cat;
            axios.get(this.host+'/details/'+this.cat+'/?page_size='+ this.page_size, {
            responseType:'json'
            })
            .then(response => {
                // 头条列表数据
                this.detail_list = response.data.results;
                this.page_max = response.data.count/(this.page_size)
            })
            .catch(error => {
                console.log(error.response.data)
            });

            // 改变红色部分的选择
            this.current=index;
        },
        enter(){
            this.display = 'block';
        },
        leave(){
            this.display = 'none';
        }
    }
});