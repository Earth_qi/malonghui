let vm = new Vue({
    el: "#app",
    data: {
        host: host,
        // 后期用户登录
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        // 提交的内容
        title: "",
        tags: "",
        content: {},
    },
    mounted: function () {
        if (this.user_id && this.token) {
            CKEDITOR.replace('editor1');
        } else {
            location.href = './person-register.html'
        }
    },
    methods: {
        on_submit: function () {
            this.content = CKEDITOR.instances.editor_text.getData();
            //             {
            //     "title": "xxxx",
            //     "tags": "python,java,xxx",
            //     "content": "xxxxxxxxxx"
            // }
            axios.post(this.host + '/questions/submission/', {
                "title": this.title,
                "tags": this.tags,
                "content": this.content
            }, {
                headers: {
                    "Authorization": "JWT " + this.token
                },
                responseType: "json"
            })
                .then(response => {
                    let id = response.data.id
                    location.href = '/qa-detail.html?id=' + id;
                })
                .catch(error => {
                    alert('用户未登录')
                })
        }
    }
});